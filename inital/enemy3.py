# Final Boss
import soy
import datetime


def changeRoomGravityRandomly(room):
    gravityStrength = 7.0
    # if player is close enough to the boss, mess with gravity
    if(room['player'].position.x > 100):
        randomNumber = int(str(datetime.datetime.now().microsecond)[:1])
        if (randomNumber > 8):
            reverseGravity = soy.fields.Accelerate(soy.atoms.Vector((0, 9.8 * gravityStrength , 0)))
            room.addField('reverseGravity', reverseGravity)
            room.removeField('gravity')
        else: # Randomnumber < 8
            gravity = soy.fields.Accelerate(soy.atoms.Vector((0, -9.8 * gravityStrength, 0)))
            room.addField('gravity', gravity)
            room.removeField('reverseGravity')
    return

def createEnemy(room) :

    reference = soy.bodies.Box()
    reference.position = soy.atoms.Position((115, -6, 7))
    reference.size = soy.atoms.Size((2, 5, 2))
    reference.material = soy.materials.Colored()
    reference.material.ambient = soy.atoms.Color('black')
    reference.material.diffuse = soy.atoms.Color('yellow')
    reference.material.specular = soy.atoms.Color('yellow')

    room['enemy3'] = reference


    enemyY = -5.98
    enemyXStart = 110
    enemyXFinish = 120
    enemyZ = 7
    path4 = (soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ)),
            soy.atoms.Position((enemyXStart, enemyY, enemyZ)),
            soy.atoms.Position((enemyXFinish, enemyY, enemyZ))
            )

    cont = soy.controllers.Pathfollower(room, room['enemy3'] , path4, 3)
    return