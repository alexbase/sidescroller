# Custom Level Exporter for Blender 2.78
# Exports all objects in scene with normals and UV cords - check README.txt
import bpy
import bmesh
from mathutils import Vector

sceneName = bpy.path.basename(bpy.context.blend_data.filepath).split(".")[0]
exportFileLocation = '/home/alex/Documents/SideScroller/customBlenderExporter/level1/' 

# First open <scene>Models.py - Appends the vertices together
modelsFileLocation = exportFileLocation + sceneName + "Models.py"
fi = open(modelsFileLocation, 'w')

# Second file open, <scene>ModelLoader.py - Loads the models into pysoy at the specified location in blender
modelLoaderFlleLocation = exportFileLocation + sceneName + "ModelLoader.py"
modelLoaderFile = open(modelLoaderFlleLocation, 'w')

# Writing Start of <scene>Models.py
fi.write("import soy" + "\n")
fi.write("\n")


# Writing Start of <scene>ModelLoader.py 
modelLoaderFile.write("import soy" + "\n")
modelLoaderFile.write("from .level1Models import *" + "\n")
modelLoaderFile.write("def " + sceneName + "LoadModels(room, textures): " + "\n")

# Iteratng through all object in the scene only they are a mesh
for obj in bpy.context.scene.objects:
    if obj.type != 'MESH':
        continue
    
    # Creatng function and starting mesh settngs
    fi.write("def add" + obj.name +"(room, key, position, textures):" + "\n")
    fi.write("    room[key] = soy.bodies.Mesh()" + "\n")

    fi.write("    matIndex0 = soy.materials.Colored(\"brown\")" + "\n") 
    fi.write("    room[key].rotation = soy.atoms.Rotation((0, 0, 1.5))" + "\n")

    fi.write("    room[key].position = position" + "\n")
    fi.write("    room[key].toggleField()" + "\n")
    
    #Creating function contain the vertces for each object

    #obj = bpy.context.active_object
    me = obj.data
    uv_layer = me.uv_layers.active.data
    i = 0

    #Creatng UV dataStructures to store in vertex index
    uvList = list()

    # Gathering uv cords nto uvList for later use
    currentV = 0
    for poly in me.polygons:
        for loop_index in range(poly.loop_start, poly.loop_start + poly.loop_total):
            uvList.append(uv_layer[loop_index].uv)
            currentV += 1 
        
    # Exporting Vertices
    currentV = 0
    for f in obj.data.polygons:
        j = 0
        for idx in f.vertices:
            fi.write("    v" + str(currentV) + " = soy.atoms.Vertex(soy.atoms.Position((" + str(obj.data.vertices[idx].co[0]))
            fi.write("," + str(obj.data.vertices[idx].co[1]))  
            fi.write("," + str(obj.data.vertices[idx].co[2]) + ")),")
            fi.write("soy.atoms.Vector((")
            fi.write(str(obj.data.vertices[idx].normal.x) + ",")
            fi.write(str(obj.data.vertices[idx].normal.y) + ",")
            fi.write(str(obj.data.vertices[idx].normal.z) + ")),")                
            fi.write("soy.atoms.Position((")
            fi.write(str(uvList[currentV][0]) + "," + str(uvList[currentV][1]))
            fi.write(")),")                  
            fi.write("soy.atoms.Vector((0, 0, 0))) \n")
            j += 1
            currentV += 1
        i += 1
    
    i = 0  
    currentV = 0  
    for f in obj.data.polygons:
        j = 0
        fi.write("    f" + str(i) + " = soy.atoms.Face(")
        for idx in f.vertices:
            fi.write("v" + str(currentV) + ",")
            currentV += 1
            if j == 2 :
                break
            j += 1
        # Writing he current vertex group as material stand in - join is used to get red of possible white space, remove if unessary
        fi.write(( "matIndex"+ "".join(str(f.material_index).split())) + ")\n")
        fi.write("    room[key].append(f" + str(i) + ")" + "\n")
        i += 1

    for f in obj.data.polygons: 
        i += 1   
    me = bpy.context.object.data

    # Adding return to object creation function
    fi.write("    return" + "\n")

    #Creatng Entry in <scene>ModelLoader.py
    modelLoaderFile.write("    add" + obj.name +"(room,\"" + obj.name + "\", soy.atoms.Position((")
    #Swapping y and -z because PySoy is Y up, and Blender is Z up
    modelLoaderFile.write( str(obj.location.x) + "," + str( -1 * obj.location.z) + "," + str(obj.location.y) + ")), textures)" + "\n") 
          
# Adding return to <scene>ModelLoader.py
modelLoaderFile.write( "    return" + "\n")           

#Closing both opened fles
fi.close()   
modelLoaderFile.close() 


