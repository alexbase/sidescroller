## Working Example ##
Look in the Blender file level1.blend for a working example.

## singleobjectExporter.py ##
This exports a single selected object into python code than PySoy understands. 

1. Select the object you want to export in the view port. Make sure the mesh is only made up of triangles.
2. Run the singleobjectExporter.py script in blender. Make sure you are in object mode.
3. Result is put where the file is opened to at the start of the script

## levelExport.py ##
This exports the entire blender scene into python code. The result is split into two parts.

   	<scene>Models.py 	# Appends the vertices together 
	<scene>ModelLoader.py	# Loads the models into pysoy at the specified location in blender
	<scene>MovementEnforcer.py	# Freezes object not supposed to be moved. These objects have "NoMove" at the start of their name in blender.

To load the models from the main thread of your game, import <scene>ModelLoader and call
	
	<scene>ImportModels()

1. Run the levelExport.py
2. Find result in the specified directory at the start of the script



