# Gameplay Loop - Camera Functions

import soy
import sys


# Camera Follow Script
def cameraAlignWithPlayer(room, cameraScrollSpeedX, cameraScrollSpeedY, debug, menuOpen):
    # Exiting early if menu is open
    if menuOpen == 1:
        room['cam'].velocity = soy.atoms.Vector((0, 0, 0))
        room['cam'].position = soy.atoms.Position((0, -3, 32))
        return
    if (debug):
        sys.stdout.write("TRying to move camera" + "\n")
        sys.stdout.flush()
    camX = room['cam'].position.x
    playerX = room['player'].position.x
    camY = room['cam'].position.y
    playerY = room['player'].position.y
    cameraDistanceBeforeMoving = 2
    # Bringing the billboard along with the camera
    if (debug):
        sys.stdout.write("Camera" + str(room['player'].position) + "\n")
        sys.stdout.flush()

    # Checking values so they aren't repeatedly checked in if statement
    XValueMatched = 1
    YValueMatched = 1
    if abs(camX - playerX) > cameraDistanceBeforeMoving:
        XValueMatched = 0
    if abs(camY - playerY) > cameraDistanceBeforeMoving:
        YValueMatched = 0
    XcamPlayerDifferMoreThan0 = 0
    YcamPlayerDifferMoreThan0 = 0
    if (camX - playerX) > 0:
        XcamPlayerDifferMoreThan0 = 1
    if (camY - playerY) > 0:
        YcamPlayerDifferMoreThan0 = 1

    #Smooth rotate - Too choppy, Seems to subtract from the player's experience, consider removing
    # camRotateSpeed = 0.1
    # camRotateSpeedBack = 0.05
    # sys.stdout.write("Cam rotation" + str(room['cam'].rotation.y) + "\n")
    # sys.stdout.flush()
    # if XValueMatched == 0:
    #    if XcamPlayerDifferMoreThan0:
    #         room['cam'].angular_velocity = soy.atoms.Vector((0, camRotateSpeed, 0))
    #    else:
    #         room['cam'].angular_velocity = soy.atoms.Vector((0, -camRotateSpeed, 0))
    # else:
    #     # Set the camera rotation back to horizontal
    #     if abs(room['cam'].rotation.y) < 0.05:
    #          room['cam'].angular_velocity = soy.atoms.Vector((0, 0, 0))
    #     else:
    #         if XcamPlayerDifferMoreThan0:
    #             room['cam'].angular_velocity = soy.atoms.Vector((0, -camRotateSpeedBack, 0))
    #         else:
    #             room['cam'].angular_velocity = soy.atoms.Vector((0, camRotateSpeedBack, 0))

    # Checking to see if the y value needs to be adjusted - Smooth follow
    if XValueMatched == 0:
        if YValueMatched == 0:
            if XcamPlayerDifferMoreThan0:
                if YcamPlayerDifferMoreThan0:
                    room['cam'].velocity = soy.atoms.Vector((-cameraScrollSpeedX, -cameraScrollSpeedY, 0))
                else:
                    room['cam'].velocity = soy.atoms.Vector((-cameraScrollSpeedX, cameraScrollSpeedY, 0))
            else:
                if YcamPlayerDifferMoreThan0:
                    room['cam'].velocity = soy.atoms.Vector((cameraScrollSpeedX, -cameraScrollSpeedY, 0))
                else:
                    room['cam'].velocity = soy.atoms.Vector((cameraScrollSpeedX, cameraScrollSpeedY, 0))
        else:
            if XcamPlayerDifferMoreThan0:
                if YcamPlayerDifferMoreThan0:
                    room['cam'].velocity = soy.atoms.Vector((-cameraScrollSpeedX, 0, 0))
                else:
                    room['cam'].velocity = soy.atoms.Vector((-cameraScrollSpeedX, 0, 0))
            else:
                if YcamPlayerDifferMoreThan0:
                    room['cam'].velocity = soy.atoms.Vector((cameraScrollSpeedX, 0, 0))
                else:
                    room['cam'].velocity = soy.atoms.Vector((cameraScrollSpeedX, 0, 0))
    else:
        if YValueMatched == 0:
            if YcamPlayerDifferMoreThan0:
                room['cam'].velocity = soy.atoms.Vector((0, -cameraScrollSpeedY, 0))
            else:
                room['cam'].velocity = soy.atoms.Vector((0, cameraScrollSpeedY, 0))
        else:
            room['cam'].velocity = soy.atoms.Vector((0, 0, 0))
    return
