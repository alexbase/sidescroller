import soy

def createEnemy(room) :

    reference = soy.bodies.Box()
    reference.position = soy.atoms.Position((10, -3, -3))
    reference.size = soy.atoms.Size((2, 4, 2))
    reference.material = soy.materials.Colored()
    reference.material.ambient = soy.atoms.Color('red')
    reference.material.diffuse = soy.atoms.Color('yellow')
    reference.material.specular = soy.atoms.Color('yellow')

    room['enemy'] = reference

    enemyX = 9.1
    enemyY = -5.98
    enemyZStart = 0
    enemyZFinish = 14
    path = (soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)),
            soy.atoms.Position((10, enemyY, enemyZStart)),
            soy.atoms.Position((10, enemyY, enemyZFinish)))

    cont = soy.controllers.Pathfollower(room, room['enemy'] , path , 3)
    return
