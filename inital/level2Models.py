import soy

def addRP_Cube2(room, key, position, rotation, textures):
    room[key] = soy.bodies.Box()
    room[key].size = soy.atoms.Size((67.70691680908203,34.303199768066406,0.8629851937294006))
    matIndex0 = soy.materials.Textured()
    matIndex0.colormap = textures['wood']
    room[key].rotation = rotation
    room[key].toggleField()
    room[key].material = matIndex0
    room[key].density = 100
    return
def addRP_Cube(room, key, position, rotation, textures):
    room[key] = soy.bodies.Box()
    room[key].size = soy.atoms.Size((17.817655563354492,19.171899795532227,0.8629851937294006))
    matIndex0 = soy.materials.Textured()
    matIndex0.colormap = textures['wood']
    room[key].rotation = rotation
    room[key].toggleField()
    room[key].material = matIndex0
    room[key].density = 100
    return
