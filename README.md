# Google Summer of Code Final Evaluation:
All the code is this repository was written for a Google Summer of Code 2017 project for Copyleft Games. This repository also contains an Blender file which contains the models used in the game. This Blender file also shows how to use the Blender to PySoy exporter that was written for this project. 
This project can be run by following the instructions below.

# How to run:
1. Navigate to the directory SideScoller is extracted to.

2. Run the command 
	$ python3 SideScroller.py

