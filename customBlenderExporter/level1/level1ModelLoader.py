import soy
from .level1Models import *
def level1LoadModels(room, textures): 
    addPlane(room,"Plane", soy.atoms.Position((-66.14596557617188,-0.7702682018280029,32.6130256652832)), textures)
    addCube(room,"Cube", soy.atoms.Position((0.0,-0.0,0.0)), textures)
    addCube2(room,"Cube2", soy.atoms.Position((-4.766434669494629,-0.0,-0.5589218139648438)), textures)
    return
