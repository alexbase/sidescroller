# Gameplay Loop - Menu Selection Functions

import soy
import sys


def updateScore(client, room, menuOpen):
    if not menuOpen:
        score = int(room['player'].position.x) + 60
        client.window.title = "Score: " + str(score)
    return
