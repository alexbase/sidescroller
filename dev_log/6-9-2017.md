# Dev Log 7-9-2017:

This week I worked on improving my custom Blender exporter and modeling parts of the second level. The exporter is a python script I developed to fit my own use case. It allows me to use Blender as a level editor and has a support for a variety of different PySoy specific features. The plugin can export color information, an object as a mesh, or an object as a PySoy box. If anyone else want to be able to use the plugin, let me know and I can refactor it into a full Blender plugin. 

In addition, I worked on creating some methods to prepare to load the second level when the player reaches the end of the first level.
