import soy
def freezeMesh(item, position, rotation):
    item.velocity = soy.atoms.Vector((0,0,0))
    item.angular_velocity = soy.atoms.Vector((0,0,0))
    item.position = position
    item.rotation = rotation
    return
def freezeSceneMeshes(room):
    freezeMesh(room["NoMove_Torch"], soy.atoms.Position((58.62819290161133,0.911569356918335,-3.988997459411621)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["NoMove_Spike"], soy.atoms.Position((57.01658630371094,-21.715717315673828,3.4620604515075684)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["NoMove_Rampart_2"], soy.atoms.Position((21.260873794555664,-9.142654418945312,-7.547886848449707)),soy.atoms.Rotation((-1.0149556398391724,-0.0,1.5708)))
    freezeMesh(room["DP_Tree_1"], soy.atoms.Position((-72.2793197631836,-7.708539009094238,-4.19899320602417)),soy.atoms.Rotation((10.95671272277832,0.0,1.5708)))
    freezeMesh(room["NoMove_Tree"], soy.atoms.Position((-66.5472412109375,-6.734559535980225,-8.354089736938477)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["NoMove_GroundPlane"], soy.atoms.Position((-21.103275299072266,-22.556615829467773,-51.379737854003906)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["RP_Cube"], soy.atoms.Position((-60.63926696777344,-8.715718269348145,1.7081859111785889)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["NoMove_Mountain3"], soy.atoms.Position((-62.044456481933594,-18.535690307617188,-63.39704513549805)),soy.atoms.Rotation((0.032474443316459656,0.0,1.5708)))
    freezeMesh(room["RP_Cube2"], soy.atoms.Position((-38.167579650878906,-8.715718269348145,1.7081859111785889)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["RP_Cube3"], soy.atoms.Position((-2.410449981689453,-8.715718269348145,1.7081859111785889)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["RP_Cube4"], soy.atoms.Position((-25.798484802246094,-14.254152297973633,1.7081859111785889)),soy.atoms.Rotation((0.0,-4.723508358001709,1.5708)))
    freezeMesh(room["RP_Cube5"], soy.atoms.Position((-7.9078240394592285,-0.6940286755561829,2.290714979171753)),soy.atoms.Rotation((0.0,-4.723508358001709,1.5708)))
    freezeMesh(room["RP_Cube6"], soy.atoms.Position((-6.649463653564453,11.53433609008789,1.7081859111785889)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["NoMove_Mountain5"], soy.atoms.Position((-49.77031707763672,-22.739730834960938,-33.95873260498047)),soy.atoms.Rotation((-0.3485445976257324,0.0,1.5708)))
    freezeMesh(room["RP_Cube7"], soy.atoms.Position((28.22273826599121,-9.810164451599121,1.051854133605957)),soy.atoms.Rotation((0.04500490799546242,-0.0,1.5708)))
    freezeMesh(room["NoMove_Rampart7"], soy.atoms.Position((44.2437629699707,-9.539793014526367,-11.78560733795166)),soy.atoms.Rotation((-1.2900867462158203,-0.0,1.5708)))
    freezeMesh(room["NoMove_Rampart"], soy.atoms.Position((36.5465087890625,-9.142654418945312,-8.940658569335938)),soy.atoms.Rotation((-1.0149556398391724,-0.0,1.5708)))
    freezeMesh(room["DP_Tree_3"], soy.atoms.Position((-70.6655044555664,-9.291507720947266,5.375668525695801)),soy.atoms.Rotation((10.660679817199707,-0.0,1.5708)))
    freezeMesh(room["DP_Tree_2"], soy.atoms.Position((-72.87004089355469,-9.881016731262207,-13.431558609008789)),soy.atoms.Rotation((11.018576622009277,0.0,1.5708)))
    freezeMesh(room["RP_Cube10"], soy.atoms.Position((48.25489807128906,-0.9405708312988281,7.873215675354004)),soy.atoms.Rotation((1.1083837747573853,-4.723501205444336,1.1470017965316772)))
    freezeMesh(room["WallRP_FarWall"], soy.atoms.Position((72.93769836425781,-2.506559371948242,-5.845353126525879)),soy.atoms.Rotation((0.010057556442916393,-6.2879109382629395,0.015405112075805638)))
    freezeMesh(room["RP_Cube12"], soy.atoms.Position((46.80036926269531,-8.715718269348145,1.7081859111785889)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["NoMove_Spike2"], soy.atoms.Position((57.01658630371094,-21.715717315673828,-0.7379393577575684)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["NoMove_Spike3"], soy.atoms.Position((59.81658935546875,-21.715717315673828,2.0620603561401367)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["RP_Cube20"], soy.atoms.Position((71.58601379394531,-8.715718269348145,1.948185920715332)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["RP_Cube20Stair"], soy.atoms.Position((78.91835021972656,-10.039788246154785,1.948185920715332)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["RP_Cube20Stair2"], soy.atoms.Position((80.21417236328125,-11.338637351989746,1.948185920715332)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["RP_Cube20Stair3"], soy.atoms.Position((81.31651306152344,-12.756731033325195,1.948185920715332)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["RP_Cube20Stair4"], soy.atoms.Position((82.22946166992188,-14.330756187438965,1.948185920715332)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["RP_Cube21"], soy.atoms.Position((87.62171936035156,-15.635868072509766,1.948185920715332)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["NoMove_Torch2"], soy.atoms.Position((86.30277252197266,-9.86267375946045,-3.988997459411621)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["CaveRP_CaveFloor"], soy.atoms.Position((105.27403259277344,-15.635868072509766,3.7153804302215576)),soy.atoms.Rotation((0.0,0.0,1.5708)))
    freezeMesh(room["CaveRP_Cave"], soy.atoms.Position((104.29103088378906,-4.5400390625,-3.410011053085327)),soy.atoms.Rotation((0.010057556442916393,-6.2879109382629395,0.015405112075805638)))
    return
